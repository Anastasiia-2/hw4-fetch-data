getHeroStars();

function getHeroStars() {
  const API_URL = "https://ajax.test-danit.com/api/swapi/films";

  return fetch(API_URL)
    .then(response => response.json())
    .then(result => {
      const filmList = document.querySelector(".film-list");
      let fragment = "";
      result.forEach(film => (fragment += renderFilmTemplate(film)));

      filmList.insertAdjacentHTML("beforeend", fragment);
      return result;
    })
    .then(result => {
      result.forEach(async film => {
        const domFilm = document.getElementById(`${film.id}`);
        const filmTitle = domFilm.firstElementChild;

        const herosNames = await film.characters.map(url => {
          return getHeros(url);
        });
        Promise.all(herosNames).then(name => {
          filmTitle.nextElementSibling.classList.remove("spiner");
          filmTitle.insertAdjacentHTML(
            "afterend",
            `<span class="film__text film__text--accent">Кіноперсонажі:</span> ${name}`
          );
        });
      });
    })
    .catch(err => console.error(err));
}

function renderFilmTemplate({ id, episodeId, name, openingCrawl }) {
  return `<li id=${id} class="film__card">
            <h2 class="film__title">${name}</h2>
            <div class="spiner"></div>
            <p class="film__text"><span class="film__text film__text--accent">Номер епізоду:</span> ${episodeId}</p>
            <p class="film__text">${openingCrawl}</p>
          </li>`;
}

function getHeros(url) {
  return fetch(url)
    .then(response => response.json())
    .then(hero => hero.name);
}
